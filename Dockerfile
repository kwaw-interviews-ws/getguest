# use official python3.9 base image
FROM python:3.9-alpine

# expose port for python application to listen on the container
EXPOSE 5000

# create working directory files and add files
WORKDIR /getguest
COPY . /getguest

# install python requirements
RUN pip install -r requirements.txt


# run application
ENTRYPOINT ["python", "app.py"]

