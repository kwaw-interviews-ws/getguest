from __future__ import annotations

import typing as t

import datetime

from contextlib import contextmanager
from contextlib import closing
from collections import OrderedDict
from dataclasses import dataclass


import mysql.connector


class Database:
    """

    Database wrapper for connect to mysql.

    """

    def __init__(self, *args, **kwargs) -> None:
        """

        args
            Arguments to create a connection to the database.

        kwargs
            Keywords arguments to create a connection to the database. It is strongly recommended
            that you only use keyword parameters.

        """
        self.args = args
        self.kwargs = kwargs

        self._connection = None

    @property
    def connection(self) -> None:
        """
        Create a connection to the mysql database with self.args
        and self.keyword. Usage as a context manager will only
        close connection at exist.

        """
        self._connection = mysql.connector.connect(*self.args, **self.kwargs)
        return self._connection

    @contextmanager
    def transact(self) -> None:
        """

        Context manager to create a database transaction
        Explicitly BEGIN the transaction if auto commit is enabled.
        Will close connection and connection cursor on exist.

        """
        with closing(self.connection) as connection:
            cursor = connection.cursor(dictionary=True)
            yield cursor
            connection.commit()

    def execute(
        self,
        query: str,
        args: t.Union[t.Dict[str, str], t.List[str], None] = None,
    ) -> None:
        """

        Execute a query.

        query
        string, query to execute on server
        args
        optional sequence or mapping, parameters to use with query.

        """
        with self.transact() as cursor:
            cursor.execute(query, args)

            return cursor

    def executemany(
        self, query: str, args: t.Union[t.Dict[str, str], t.List[str]]
    ) -> None:
        """

        Execute a multi-row query.

        query
        string, query to execute on server
        args
        sequence or mapping, parameters to use with query.

        """
        with self.transact() as cursor:
            cursor.executemany(query, args)

            return cursor

    def table_exists(self, table_name: str) -> bool:
        query = "SHOW TABLES LIKE %s"
        with self.transact() as cursor:
            cursor.execute(query, (table_name,))
            return bool(cursor.fetchone())


class ModelMeta(type):
    """

    Meta programmer for database relational mapper.
    Create a new model class decorated by a dataclass.

    """

    def __new__(
        cls,
        name: str,
        bases: t.Tuple[type, ...],
        attrs: t.Dict[str, t.Any],
    ) -> ModelMeta:
        new_cls = super().__new__(cls, name, bases, attrs)
        new_cls = dataclass(new_cls)

        return new_cls


class Model(metaclass=ModelMeta):
    """

    Database relational mapping.

    """

    database = None
    model_registry = OrderedDict()

    def __init_subclass__(cls, **kwargs) -> None:
        super().__init_subclass__(**kwargs)

        Model.model_registry.setdefault(cls.__name__, cls)

    @classmethod
    def manager(cls, database=None):
        return Manager(database or cls.database, cls)

    def insert(self):
        self.manager().insert(self)

    def update(self):
        self.manager().update(self)

    def delete(self):
        self.manager().delete(self)


class Manager:
    """

    Database driver from mysql.

    """

    TYPES_MAPPING = {
        str: "VARCHAR(225)",
        int: "INT",
        float: "FLOAT",
        datetime.datetime: "DATETIME",
    }

    def __init__(self, database: Database, model: Model) -> None:
        self.database = database
        self.model = model

        if not self.database.table_exists(self.table_name):
            self.create_table()

    def _modelize(self, **kwargs) -> Model:
        """

        Helper to convert keywords arguments into a model instance.
        If keywords contains 'id' remove and add after model
        object is created.

        """
        id = kwargs.pop("id", None)
        model = self.model(**kwargs)
        model.id = id

        return model

    @classmethod
    def _where(cls, model=None, **kwargs) -> str:
        """

        Helper to convert model and keyword arguments to a database
        where clause.

        """
        if kwargs:
            where = "WHERE"
            ands = " AND ".join(f"{k}=%({k})s" for k, v in kwargs.items())
            return f"{where} {ands}"
        return ""

    @property
    def table_name(self) -> str:
        """

        Get the database table name from self.model name.
        Returns the table name as a lower case of the
        self.model name.

        """
        return self.model.__name__.lower()

    def get_model_fields(self) -> dict:
        """

        Get fields set on the realted model as a dict

        """
        return dict(
            (k, v.type)
            for k, v in vars(self.model)["__dataclass_fields__"].items()
        )

    def get_model_columns(self) -> list:
        """

        Get related model fields and as a translated
        as database columns.

        """
        types = self.TYPES_MAPPING

        def get_type(v):
            try:
                typ = types[v]
            except KeyError:
                typ = types[eval(v)]
            return typ

        model_attrs = self.get_model_fields().items()
        model_attrs = {k: v for k, v in model_attrs}
        return [
            f"`{k}` {get_type(v)} DEFAULT NULL" for k, v in model_attrs.items()
        ]

    def create_table(self) -> None:
        """

        Create database table

        """
        columns = ", ".join(self.get_model_columns())
        create_table_query = (
            f"CREATE TABLE `{self.table_name}` "
            f"(id INT(11) unsigned NOT NULL AUTO_INCREMENT, "
            f"{columns}, PRIMARY KEY (id)) "
            "ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8"
        )
        self.database.execute(create_table_query)

    def getall(self, **kwargs) -> t.List[Model]:
        """

        Fetch all in database table.
        Keyword arguments are converted into a where query clause.

        """

        with self.database.transact() as cursor:
            query = f"SELECT * FROM {self.table_name} {self._where(**kwargs)}"
            cursor.execute(query, kwargs)
            return [self._modelize(**r) for r in cursor.fetchall()]

    def get(self, **kwargs) -> t.Optional[Model]:
        """

        Fetch first one item in database table.
        Keyword arguments are converted into a where query clause.

        """
        with self.database.transact() as cursor:
            query = f"SELECT * FROM {self.table_name} {self._where(**kwargs)}"
            cursor.execute(query, kwargs)
            row = cursor.fetchone()
            if row:
                return self._modelize(**row)

    def insert(self, model) -> Model:
        """

        Insert model in database table.
        If model has an id, it has already been save, raises a ValueError.

        """
        if model.__dict__.get("id") is not None:
            raise ValueError("object already exist")

        model_values = list(model.__dict__.values())

        fields = self.get_model_fields()
        field_names, field_types = fields.keys(), fields.values()
        fields_string = ", ".join(f"`{_}`" for _ in field_names)
        values_string = ", ".join(_ for _ in ("%s",) * len(field_types))
        query = f"INSERT INTO {self.table_name} ({fields_string}) VALUES ({values_string})"
        cursor = self.database.execute(query, model_values)

        model.id = cursor.lastrowid
        return model

    def update(self, model) -> None:
        """

        Modify the existing records in a database table
        If model does not have an id then it does not exist in database,
        raises a ValueError.

        """
        if model.__dict__.get("id") is None:
            raise ValueError("object already exist")

        field_names = self.get_model_fields().keys()

        update_set = ", ".join(f"`{name}`=%({name})s" for name in field_names)
        query = f"UPDATE {self.table_name} SET {update_set} WHERE id=%(id)s"
        self.database.execute(query, model.__dict__)

    def delete(self, model) -> None:
        """

        Modify the existing records in a database table.
        If model does not have an id then it does not exist in database,
        raises a ValueError.

        """
        if model.__dict__.get("id") is None:
            raise ValueError("object already exist")

        query = f"DELETE FROM {self.table_name} WHERE id=%(id)s"
        self.database.execute(query, model.__dict__)

    def query(
        self,
        query: str,
        args: t.Union[t.Dict[str, str], t.List[str], None] = None,
    ):
        """
        Execute a query.

        query, query statement to execute on server
        args, optional sequence or mapping, parameters to use with query.

        """
        with self.database.transact() as cursor:
            cursor.execute(query, args)
            results = cursor.fetchall()

        return [self._modelize(**r) for r in results]
