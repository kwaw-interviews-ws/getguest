from __future__ import annotations

import typing as t


import types
import operator


from .exceptions import SerializerException


class Field:
    """

    Define what attribute to be serialized on a Serializer class.

    name is the attribute name on serializing object.
    label is for labeling the serialized field
    required, if truly and and value is falsely raises error

    """

    def __init__(
        self, name: str = None, label: str = None, required: bool = True
    ) -> None:
        self.name = name
        self.label = label
        self.required = required

    def to_native(self, value):
        """

        Convert to a field native value.
        Value is from the serializing object.

        """
        return value

    def getter(
        self, serializer_field_name: str, serializer_cls: "Serializer"
    ) -> None:
        """

        Factor for getting attributes from a serializing object.
        Returns None with use default getter set on serializing class.

        """
        return None


class StringField(Field):
    """

    Field to serialize to string.

    """

    to_native = staticmethod(str)


class IntegerField(Field):
    """

    Field to serialize to integer.

    """

    to_native = staticmethod(int)


class FloatField(Field):
    """

    Field to serialize to integer.

    """

    to_native = staticmethod(float)


class DateTimeField(Field):
    """

    Field to serialize to datetime.datetime.

    """

    def to_native(self, value):
        if value is not None:
            return value.isoformat()


class BaseSerializer(Field):
    """

    Base serializer class

    """

    _fields = {}


class MetaSerializer(type):
    """

    Serializer meta programmer

    """

    def __new__(
        cls,
        name: str,
        bases: t.Tuple[type, ...],
        attrs: t.Dict[str, t.Any],
    ) -> MetaSerializer:
        serializer_fields = {}
        fields = {}
        processed_fields = []

        for name, field in attrs.items():
            if isinstance(field, Field):
                serializer_fields[name] = field

        for _ in serializer_fields:
            attrs.pop(_)

        new_cls = super().__new__(cls, name, bases, attrs)

        for cls in new_cls.__mro__[::-1]:
            if issubclass(cls, BaseSerializer):
                fields.update(cls._fields)

        fields.update(serializer_fields)

        for name, field in fields.items():
            getattr = field.getter(name, new_cls)
            if getattr is None:
                getattr = new_cls.default_getter(field.name or name)

            to_native = field.to_native
            name = field.label or name

            processed_fields.append(
                (
                    field,
                    name,
                    getattr,
                    to_native,
                )
            )

        new_cls._fields = fields
        new_cls._processed_fields = tuple(processed_fields)

        return new_cls


class Serializer(BaseSerializer, metaclass=MetaSerializer):
    """

    Base class for serializing objects

    Instance is the object to serialize.
    Setting many to True will expect a sequence for instance.
    Can be used as a serializer field with all field parameters.

    """

    default_getter = operator.attrgetter

    def __init__(self, instance=None, many=False, **kwargs):
        super().__init__(**kwargs)

        self.instance = instance
        self.many = many

        self._data = None

    def get_fields(self):
        """

        Get the fields set on the serializing class.

        """
        return self._fields

    def to_native(
        self, instance: t.Union[t.Any, t.List[t.Any]]
    ) -> t.Union[t.Dict[str, t.Any], t.List[t.Dict[str, t.Any]]]:
        """

        Trigger main serializing logic.

        """
        if self.many:
            return [self._serialize(i) for i in instance]
        return self._serialize(instance)

    def data(self) -> t.Union[dict, t.List[dict]]:
        """

        Lazy loaded for getting serialized data.

        """
        if not self._data:
            self._data = self.to_native(self.instance)

        return self._data

    def _serialize(self, instance) -> t.Union[dict, t.List[dict]]:
        """

        Main serializing logic.
        Instance of the object to serialize in as a sequence of
        objects or a single object.

        """
        serialized = {}

        for (
            field,
            name,
            getter,
            to_native,
        ) in self._processed_fields:
            required = field.required

            if isinstance(field, BaseSerializer):
                instance = getter(instance)
                if field.instance is None:
                    field.instance = instance
                result = field.data()
            else:
                try:
                    result = getter(instance)
                except (KeyError, AttributeError) as ex:
                    if required:
                        raise SerializerException(ex)
                    else:
                        continue
                if required or result is not None:
                    if to_native:
                        try:
                            result = to_native(result)
                        except TypeError as ex:
                            raise SerializerException(ex)

            serialized[name] = result

        return serialized
