from __future__ import annotations

import typing as t
import datetime

from flask import current_app

from getguest.wrappers.database import Model
from getguest.wrappers.database import Database
from getguest.wrappers.database import Manager

from getguest.exceptions import ModelException


def init_db():
    database = Database(
        host=current_app.config["DB_HOST"],
        port=current_app.config["DB_PORT"],
        user=current_app.config["DB_USER"],
        password=current_app.config["DB_PASSWORD"],
        database=current_app.config["DB_NAME"],
    )
    return database


class BaseModel(Model):
    database = init_db()


class GuestListManager(Manager):
    def get_arrived_list(self) -> t.List[GuestList]:
        """

        Guestlist with time arrived set and no time departed.

        """
        arrived_list = self.query(
            f"select * from {self.table_name} "
            f"where time_arrived is not null"
        )

        return arrived_list

    def get_empty_seats(self) -> int:
        """

        Seats with no occupancy.
        Returns a sum of each guestlist empty seats.

        """
        return sum(guest_list.get_empty_seats() for guest_list in self.getall())


class GuestList(Model):
    name: str
    table: int
    accompanying_guests: int
    time_arrived: datetime.datetime = None
    time_departed: datetime.datetime = None

    default_guests = 1
    database = init_db()

    @classmethod
    def manager(cls, database=None):
        return GuestListManager(database or cls.database, cls)

    def get_empty_seats(self) -> int:
        """

        Returns number of empty seats.

        If guest has not arrived or has departed then the table is empty.
        Otherwise count accompanying guests and guest.

        """
        if not self.time_arrived or self.time_departed:
            return self.table

        return self.table - (self.accompanying_guests + self.default_guests)

    def create(self) -> None:
        """

        Business logic to create a guests list.

        If accompanying_guests is none save it as 0. Requires
        number of tables to less than accompanying guests +
        main guest else raises error. Handles uniqueness of name
        at application level here.

        """
        self.accompanying_guests = self.accompanying_guests or 0
        try:
            if not self.table:
                raise ModelException("Table number is required")

            if self.accompanying_guests < 0:
                raise ModelException("Accompanying guests must be unsigned")

            if self.default_guests + self.accompanying_guests > self.table:
                raise ModelException("Table can not accommodate extra guests")

            if self.manager().get(name=self.name):
                raise ModelException("Guest is already registered")
        except Exception as ex:
            raise ModelException(ex)
        else:
            self.insert()

    def arrived(self, accompanying_guests) -> None:
        """

        Set guests arrival status.

        """
        self.accompanying_guests = accompanying_guests or 0
        if (self.accompanying_guests + self.default_guests) > self.table:
            raise ModelException("Over table number")

        self.time_arrived = datetime.datetime.now()
        self.update()

    def departed(self) -> None:
        """

        Set guests departure status.

        """
        self.time_departed = datetime.datetime.now()
        self.update()
