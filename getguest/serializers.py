from getguest.wrappers.serializer import Serializer
from getguest.wrappers.serializer import (
    StringField,
    IntegerField,
    DateTimeField,
)


class GuestListNameSerializer(Serializer):
    name = StringField()


class BaseGuestListSerializer(GuestListNameSerializer):
    table = IntegerField()
    accompanying_guests = IntegerField()


class GuestListSerializer(BaseGuestListSerializer):
    time_arrived = DateTimeField(required=False)
    time_departed = DateTimeField(required=False)


class ArrivedGuestListSerializer(GuestListNameSerializer):
    accompanying_guests = IntegerField()
    time_arrived = DateTimeField(required=False)
