import os
import logging
import datetime

from flask import Flask, Response
from flask import request, jsonify
from flask import abort


class Config:
    DEBUG = False
    TESTING = False
    DB_HOST = os.getenv("DB_HOST", "127.0.0.1")
    DB_PORT = os.getenv("DB_PORT", 3306)
    DB_USER = os.getenv("DB_USER")
    DB_PASSWORD = os.getenv("DB_PASSWORD")
    DB_NAME = "getguest"


application = Flask(__name__)
application.config.from_object(Config)


with application.app_context():
    from getguest import models, serializers
    from getguest.wrappers.exceptions import SerializerException
    from getguest.exceptions import ModelException


@application.route("/guest_list", methods=["GET"], defaults={"name": None})
@application.route("/guest_list/<name>", methods=["POST"])
def guestlist(name) -> str:
    if request.method == "GET":
        all_guest_list = models.GuestList.manager().getall()
        return jsonify(
            {
                "guests": serializers.BaseGuestListSerializer(
                    all_guest_list, many=True
                ).data()
            }
        )

    if request.method == "POST":
        payload = request.get_json()

        guest_list = models.GuestList(
            name=name,
            table=payload.get("table"),
            accompanying_guests=payload.get("accompanying_guests", 0),
        )
        guest_list.create()
        return jsonify(serializers.GuestListNameSerializer(guest_list).data())


@application.route("/guests", methods=["GET"], defaults={"name": None})
@application.route("/guests/<name>", methods=["PUT", "DELETE"])
def guests(name) -> str:
    if name:
        guestlist = models.GuestList.manager().get(name=name)
        if not guestlist:
            abort(404)

    if request.method == "PUT":
        payload = request.get_json()
        guestlist.arrived(payload.get("accompanying_guests"))

        return jsonify({"name": name})

    if request.method == "DELETE":
        guestlist.departed()

        return jsonify(), 204

    if request.method == "GET":
        arrived_guest_list = models.GuestList.manager().get_arrived_list()
        return jsonify(
            {
                "guests": serializers.ArrivedGuestListSerializer(
                    arrived_guest_list, many=True
                ).data()
            }
        )


@application.route("/seats_empty", methods=["GET"])
def seats_empty() -> str:
    empty_seats = models.GuestList.manager().get_empty_seats()

    return jsonify({"seats_empty": empty_seats})


@application.errorhandler(404)
def handler404(e):
    return jsonify({"error": {"code": 4004, "message": "Not Found"}}), 404


@application.errorhandler(SerializerException)
def handler_serializer_error(e):
    return jsonify({"error": {"code": 4001, "message": "Invalid payload"}}), 400


@application.errorhandler(ModelException)
def handler_serializer_error(e):
    return jsonify({"error": {"code": 4002, "message": str(e)}}), 400


if __name__ == "__main__":
    application.run(host="0.0.0.0", use_reloader=True)
