
# 1) Build/Run/Test
Hopefully you have Docker/Docker Compose installed

### Build and run

From root directory run the following.
```sh
docker-compose build
docker-compose up
```

### Testing

Note: depends on the former step.
```sh
docker exec -it getguest py.test -v
```

# 2) Access application
API can be accessed here - http://localhost:9096
