from dataclasses import dataclass

import pytest

from getguest.wrappers.serializer import Field
from getguest.wrappers.serializer import StringField
from getguest.wrappers.serializer import IntegerField
from getguest.wrappers.serializer import FloatField

from getguest.wrappers.serializer import Serializer

from getguest.wrappers.exceptions import SerializerException


class TestFields:
    def test_to_native_pure(self):
        field = Field()

        assert field.to_native(True) is True
        assert field.to_native(None) is None
        assert field.to_native(1) == 1
        assert field.to_native("one") == "one"

    def test_stringfield(self):
        string_field = StringField()

        assert string_field.to_native("foo") == "foo"
        assert string_field.to_native(100) == "100"
        assert string_field.to_native(True) == "True"
        assert string_field.to_native(None) == "None"

        class CustomClass:
            def __str__(self):
                return "custom_class"

        assert string_field.to_native(CustomClass()) == "custom_class"

    def test_integerfield(self):
        interger_field = IntegerField()

        assert interger_field.to_native(1) == 1
        assert interger_field.to_native(1.90) == 1
        assert interger_field.to_native(1.40) == 1
        assert interger_field.to_native(False) == 0
        assert interger_field.to_native("1") == 1

    def test_float_field(self):
        float_field = FloatField()

        assert float_field.to_native(1.5) == 1.5
        assert float_field.to_native("1.5") == 1.5


class TestSerializer:
    def test_serialize(self):
        @dataclass
        class Book:
            title: str
            author: str

        class BookSerializer(Serializer):
            title = StringField()
            author = StringField()

        book = Book(title="Test Book", author="John James")
        serialized_book = BookSerializer(book).data()

        assert serialized_book == {"title": "Test Book", "author": "John James"}

    def test_serialize_many(self):
        @dataclass
        class Book:
            title: str
            author: str

        class BookSerializer(Serializer):
            title = StringField()
            author = StringField()

        books = [
            Book(title="Test Book", author="John James"),
            Book(title="Test Book V2", author="John James"),
        ]
        serialized_books = BookSerializer(books, many=True).data()

        assert serialized_books[0] == {
            "title": "Test Book",
            "author": "John James",
        }
        assert serialized_books[1] == {
            "title": "Test Book V2",
            "author": "John James",
        }

    def test_serialize_serializer(self):
        class AddressSerializer(Serializer):
            line = StringField()
            postcode = StringField()

        class PersonSerializer(Serializer):
            name = StringField()
            age = IntegerField()
            address = AddressSerializer()

        class IdentitySerializer(Serializer):
            id = IntegerField(required=True)
            person = PersonSerializer()

        @dataclass
        class Address:
            line: str
            postcode: str

        @dataclass
        class Person:
            name: str
            age: str

            @property
            def address(self):
                return Address(line="line1", postcode="nw8")

        @dataclass
        class Identity:
            id: int

        person = Person(name="Jone", age=20)
        identity = Identity(id=129)
        identity.person = person

        serialized_person = PersonSerializer(person).data()
        serialized_identity = IdentitySerializer(identity).data()

        assert serialized_person == {
            "name": "Jone",
            "age": 20,
            "address": {"line": "line1", "postcode": "nw8"},
        }
        assert serialized_identity == {
            "id": 129,
            "person": {
                "name": "Jone",
                "age": 20,
                "address": {"line": "line1", "postcode": "nw8"},
            },
        }

    def test_serialize_serializer_many(self):
        class AddressSerializer(Serializer):
            line = StringField()
            postcode = StringField()

        class PersonSerializer(Serializer):
            name = StringField()
            age = IntegerField()
            addresses = AddressSerializer(many=True)

        @dataclass
        class Address:
            line: str
            postcode: str

        @dataclass
        class Person:
            name: str
            age: str

            @property
            def addresses(self):
                return [
                    Address(line="line1", postcode="nw8"),
                    Address(line="line2", postcode="sw1"),
                ]

        person = Person(name="Ted", age=25)

        serialized_person = PersonSerializer(person).data()

        assert serialized_person["name"] == "Ted"
        assert serialized_person["age"] == 25
        assert serialized_person["addresses"][0] == {
            "line": "line1",
            "postcode": "nw8",
        }
        assert serialized_person["addresses"][1] == {
            "line": "line2",
            "postcode": "sw1",
        }

    def test_serialize_labeling(self):
        class AddressSerializer(Serializer):
            line = StringField()
            long_postcode = StringField(label="postcode")

        class Address:
            line = "line1"
            long_postcode = "e14"

        serialized_address = AddressSerializer(Address()).data()

        assert serialized_address == {"line": "line1", "postcode": "e14"}

    def test_serialize_required(self):
        @dataclass
        class Book:
            id: int
            author: str

        @dataclass
        class IncompleteBook:
            author: str

        class BookSerializer(Serializer):
            id = IntegerField(required=True)
            author = StringField(required=False)

        # test author not required and None
        book = Book(id=100, author=None)
        serialized_book = BookSerializer(book).data()
        assert serialized_book["author"] is None

        # test id required and None
        book = Book(id=None, author="Tek")
        with pytest.raises(SerializerException):
            BookSerializer(book).data()

        # test id required and undefined
        book = IncompleteBook(author="Tek")
        with pytest.raises(SerializerException):
            BookSerializer(book).data()
