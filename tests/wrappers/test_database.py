import unittest.mock


import mysql.connector.errors

import pytest

from getguest.wrappers.database import Database
from getguest.wrappers.database import ModelMeta, Model
from getguest.wrappers.database import Manager


class TestDatabase:
    @unittest.mock.patch("mysql.connector.connect")
    def test_connection(self, mock_connection):
        connect_parameters = {"host": "myhost", "password": "mypassword"}
        Database(**connect_parameters).connection

        mock_connection.assert_called_with(**connect_parameters)

    def test_transact(self, database_connection_config):
        database = Database(**database_connection_config)

        with database.transact() as cursor:
            cursor.execute("SELECT 1")

            assert cursor.fetchone() == {"1": 1}

        with pytest.raises(mysql.connector.errors.OperationalError):
            database._connection.cursor().execute("SELECT 1")

    def test_table_exists(self, database_connection_config, person_table):
        database = Database(**database_connection_config)
        assert database.table_exists("person")

    def test_table_exists_no(self, database_connection_config):
        database = Database(**database_connection_config)

        assert not database.table_exists("person")


class TestModel:
    def test_instantiation(self):
        class Person(Model):
            name: str
            age: int
            height: int

        person_instance = Person(name="Per Son", age=30, height=130)

        assert person_instance.name == "Per Son"
        assert person_instance.age == 30
        assert person_instance.height == 130

    def test_inheritance(self):
        class Animal(Model):
            name: str
            weight: int

        class Dog(Animal):
            sound: str

        dog_instance = Dog(name="Dog", weight=50, sound="barking")

        assert dog_instance.name == "Dog"
        assert dog_instance.weight == 50
        assert dog_instance.sound == "barking"

    def test_model_registry(self):
        Model.model_registry = {}

        class Person(Model):
            name: str
            age: int
            height: int

        class Animal(Model):
            name: str
            weight: int

        class Dog(Animal):

            sound: str

        model_registry_name_list = list(Model.model_registry.keys())
        model_registry_model_list = list(Model.model_registry.values())

        assert model_registry_name_list == ["Person", "Animal", "Dog"]
        for model in model_registry_model_list:
            assert type(model) == ModelMeta


class TestManger:
    def test_table_name(self, test_database, drop_table):
        class MyModel(Model):
            field1: str
            field2: int

        manager = Manager(test_database, MyModel)
        assert manager.table_name == "mymodel"

        drop_table("mymodel")

    def test_get_model_fields(self, test_database, drop_table):
        class MyModel(Model):
            field1: str
            field2: int

        manager = Manager(test_database, MyModel)
        attributes = manager.get_model_fields()

        assert attributes["field1"] == str
        assert attributes["field2"] == int

        drop_table("mymodel")

    def test_get_model_columns(self, test_database, drop_table):
        class MyModel(Model):
            field1: str
            field2: int
            field3: float

        manager = Manager(test_database, MyModel)
        columns = manager.get_model_columns()

        assert columns == [
            "`field1` VARCHAR(225) DEFAULT NULL",
            "`field2` INT DEFAULT NULL",
            "`field3` FLOAT DEFAULT NULL",
        ]

        drop_table("mymodel")

    def test_create_table(self, test_database, drop_table):
        class PersonModel(Model):
            name: str
            weight: float

        manager = Manager(test_database, PersonModel)

        with test_database.connection as connection:
            cursor = connection.cursor()
            cursor.execute("SHOW TABLES LIKE 'personmodel'")
            assert cursor.fetchall()

        drop_table("personmodel")

    def test_helper_modelize(self, test_database, drop_table):
        class Address(Model):
            line: str
            postcode: str

        manager = Manager(test_database, Address)
        modelize_model = manager._modelize(line="line one", postcode="se01")

        assert modelize_model.line == "line one"
        assert modelize_model.postcode == "se01"

        # test with id
        modelize_model = manager._modelize(
            id=23, line="line one", postcode="se01"
        )

        assert modelize_model.id == 23

        drop_table("address")

    def test_helper_where(self):
        data = {"name": "jon", "age": 23, "weight": 34.45}
        where_clause = Manager._where(**data)

        assert (
            where_clause
            == "WHERE name=%(name)s AND age=%(age)s AND weight=%(weight)s"
        )

    def test_insert(self, test_database, drop_table):
        class Address(Model):
            line: str
            postcode: str

            database = test_database

        address = Address(line="line1", postcode="se31")
        saved_address = Address.manager().insert(address)
        queried_address = Address.manager(test_database).get(
            id=saved_address.id
        )

        assert saved_address.id is not None
        assert saved_address.id == queried_address.id
        assert saved_address.line == queried_address.line
        assert saved_address.postcode == queried_address.postcode

        drop_table("address")

    def test_insert_id_not_none(self, test_database, drop_table):
        class Address(Model):
            line: str
            postcode: str

        address = Address(line="line1", postcode="se31")
        address.id = 23

        with pytest.raises(ValueError):
            Address.manager(test_database).insert(address)

        drop_table("address")

    def test_update(self, test_database, drop_table):
        class Address(Model):
            line: str
            postcode: str

        address = Address(line="line1", postcode="se31")
        saved_address = Address.manager(test_database).insert(address)

        saved_address.line = "line2"
        saved_address.postcode = "ge14"

        Address.manager(test_database).update(saved_address)

        _id = saved_address.id
        updated_address = Address.manager(test_database).get(id=_id)

        assert updated_address.line == "line2"
        assert updated_address.postcode == "ge14"

        drop_table("address")

    def test_delete(self, test_database, drop_table):
        class Address(Model):
            line: str
            postcode: str

        address = Address(line="line1", postcode="se31")
        saved_address = Address.manager(test_database).insert(address)
        Address.manager(test_database).delete(saved_address)

        _id = saved_address.id
        deleted_address = Address.manager(test_database).get(id=_id)

        assert deleted_address is None

        drop_table("address")

    def test_update_id_undefined(self, test_database, drop_table):
        class Address(Model):
            line: str
            postcode: str

        address = Address(line="line1", postcode="se31")

        with pytest.raises(ValueError):
            Address.manager(test_database).update(address)

    def test_update_id_none(self, test_database, drop_table):
        class Address(Model):
            line: str
            postcode: str

        address = Address(line="line1", postcode="se31")
        address.id = None

        with pytest.raises(ValueError):
            Address.manager(test_database).update(address)

        drop_table("address")

    def test_delete_id_undefined(self, test_database, drop_table):
        class Address(Model):
            line: str
            postcode: str

        address = Address(line="line1", postcode="se31")

        with pytest.raises(ValueError):
            Address.manager(test_database).delete(address)

        drop_table("address")

    def test_delete_id_none(self, test_database, drop_table):
        class Address(Model):
            line: str
            postcode: str

        address = Address(line="line1", postcode="se31")
        address.id = None

        with pytest.raises(ValueError):
            Address.manager(test_database).delete(address)

        drop_table("address")
