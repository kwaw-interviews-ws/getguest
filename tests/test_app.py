"""
Integration tests from APIs
"""
from app import application

with application.app_context():
    from getguest.models import GuestList


class TestApp:
    def test_guest_list(self, test_client):

        # test create guest list
        guest_name = "jon"
        accompanying_guests = 5
        table_capacity = 6

        payload = {
            "name": guest_name,
            "accompanying_guests": accompanying_guests,
            "table": table_capacity,
        }
        response = test_client.post(f"/guest_list/{guest_name}", json=payload)
        response_data = response.get_json()

        assert response_data == {"name": guest_name}

        # test name uniqueness
        response = test_client.post(f"/guest_list/{guest_name}", json=payload)
        response_data = response.get_json()

        assert response_data["error"]["code"] == 4002

        # test too many accompanying guests
        over_payload = {
            "name": guest_name,
            "accompanying_guests": 6,
            "table": table_capacity,
        }
        response = test_client.post(
            f"/guest_list/{guest_name}", json=over_payload
        )
        response_data = response.get_json()

        assert response_data["error"]["code"] == 4002

        # tests retrieve guests list
        response = test_client.get(f"/guest_list")
        response_data = response.get_json()

        assert response_data == {
            "guests": [
                {
                    "accompanying_guests": accompanying_guests,
                    "name": guest_name,
                    "table": table_capacity,
                }
            ]
        }

    def test_guest_arrive(self, test_client):
        payload = {
            "name": "hill",
            "accompanying_guests": 5,
            "table": 6,
        }
        test_client.post("/guest_list/hill", json=payload)

        arrive_payload = {"accompanying_guests": 5}
        response = test_client.put("/guests/hill", json=arrive_payload)

        response_data = response.get_json()
        guestlist = GuestList.manager().get(name="hill")

        assert response_data == {"name": "hill"}
        assert guestlist.time_arrived

    def test_guest_arrive_with_lesser_accompanying_guests(self, test_client):
        payload = {
            "name": "hill",
            "accompanying_guests": 3,
            "table": 4,
        }
        test_client.post("/guest_list/hill", json=payload)

        arrive_payload = {"accompanying_guests": 2}
        response = test_client.put("/guests/hill", json=arrive_payload)
        response_data = response.get_json()

        assert response_data == {"name": "hill"}

    def test_guest_arrive_with_no_accompanying_guests(self, test_client):
        payload = {
            "name": "hill",
            "accompanying_guests": 3,
            "table": 6,
        }
        test_client.post("/guest_list/hill", json=payload)

        arrive_payload = {"accompanying_guests": 0}
        response = test_client.put("/guests/hill", json=arrive_payload)
        response_data = response.get_json()

        assert response_data == {"name": "hill"}

    def test_guest_arrive_with_extra_accompanying_guests(self, test_client):
        payload = {
            "name": "hill",
            "accompanying_guests": 5,
            "table": 6,
        }
        test_client.post("/guest_list/hill", json=payload)

        arrive_payload = {"accompanying_guests": 6}
        response = test_client.put("/guests/hill", json=arrive_payload)

        response_data = response.get_json()
        guestlist = GuestList.manager().get(name="hill")

        assert response_data["error"]["code"] == 4002
        assert guestlist.time_arrived is None

    def test_guest_leaves(self, test_client):
        payload = {
            "name": "hill",
            "accompanying_guests": 5,
            "table": 6,
        }
        test_client.post("/guest_list/hill", json=payload)

        response = test_client.delete("/guests/hill")
        guestlist = GuestList.manager().get(name="hill")

        assert response.status == "204 NO CONTENT"
        assert guestlist.time_departed

    def test_arrived_guests(self, test_client):
        james_payload = {
            "name": "james",
            "accompanying_guests": 3,
            "table": 4,
        }
        johns_payload = {
            "name": "johns",
            "accompanying_guests": 1,
            "table": 2,
        }

        test_client.post("/guest_list/james", json=james_payload)
        test_client.post("/guest_list/johns", json=johns_payload)

        johns_arrive_payload = {"accompanying_guests": 1}
        test_client.put("/guests/johns", json=johns_arrive_payload)

        response = test_client.get("/guests")
        response_data = response.get_json()

        guests = response_data["guests"]
        johns_guestlist = guests[0]

        assert len(guests) == 1
        assert johns_guestlist["name"] == "johns"
        assert johns_guestlist["accompanying_guests"] == 1
        assert johns_guestlist["time_arrived"]

    def test_empty_seats(self, test_client):
        # fin's table takes 6 people
        # fin is expected to come with 3 extra guests
        fin_payload = {
            "name": "fin",
            "accompanying_guests": 3,
            "table": 6,
        }
        # jane's table takes 2 people
        # jane is expected to come with 1 extra guest
        jane_payload = {
            "name": "jane",
            "accompanying_guests": 1,
            "table": 2,
        }
        # lis's table takes 10 people
        # lis is expected to come with 9 extra guests
        lis_payload = {
            "name": "lis",
            "accompanying_guests": 9,
            "table": 10,
        }
        # kik's table takes 5 people
        # kik is expected to come with 9 extra guests
        kik_payload = {
            "name": "kik",
            "accompanying_guests": 4,
            "table": 5,
        }

        # create guest list
        test_client.post("/guest_list/fin", json=fin_payload)
        test_client.post("/guest_list/jane", json=jane_payload)
        test_client.post("/guest_list/lis", json=lis_payload)
        test_client.post("/guest_list/kik", json=kik_payload)

        # fin arrives with 3 guests
        arrive_payload = {"accompanying_guests": 3}
        test_client.put("/guests/fin", json=arrive_payload)

        response = test_client.get("/seats_empty")
        response_data = response.get_json()

        assert response_data["seats_empty"] == 19

        # jane arrives with no guests
        arrive_payload = {}
        test_client.put("/guests/jane", json=arrive_payload)

        response = test_client.get("/seats_empty")
        response_data = response.get_json()

        assert response_data["seats_empty"] == 18

        # lis arrives with 9 guests
        arrive_payload = {"accompanying_guests": 9}
        test_client.put("/guests/lis", json=arrive_payload)

        response = test_client.get("/seats_empty")
        response_data = response.get_json()

        assert response_data["seats_empty"] == 8

        # kik arrives with 5 guests for a table of 5
        # so refuse entry
        arrive_payload = {"accompanying_guests": 5}
        test_client.put("/guests/kik", json=arrive_payload)

        response = test_client.get("/seats_empty")
        response_data = response.get_json()

        assert response_data["seats_empty"] == 8
