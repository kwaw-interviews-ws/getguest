import os

import pytest

import mysql.connector

from getguest.wrappers.database import Database


@pytest.fixture
def database_connection_config():
    return {
        "host": "mysql",
        "port": 3306,
        "user": "root",
        "password": "root",
        "database": "getguest",
    }


@pytest.fixture
def person_table(database_connection_config):
    create_table_stmt = """
        CREATE TABLE person (
          id int(11) unsigned NOT NULL AUTO_INCREMENT,
          name varchar(150) DEFAULT NULL,
          PRIMARY KEY (id)
        ) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
    """
    drop_table_stmt = "DROP TABLE person"
    with mysql.connector.connect(**database_connection_config) as connection:
        cursor = connection.cursor()
        cursor.execute(create_table_stmt)
        yield
        cursor.execute(drop_table_stmt)


@pytest.fixture
def drop_table(database_connection_config):
    def _drop_table(table_name):
        with mysql.connector.connect(
            **database_connection_config
        ) as connection:
            connection.cursor().execute(f"DROP TABLE IF EXISTS {table_name}")

    return _drop_table


@pytest.fixture
def test_database(database_connection_config):
    return Database(**database_connection_config)


@pytest.fixture
def test_client(database_connection_config, drop_table):
    def set_env():
        os.environ["DB_HOST"] = database_connection_config["host"]
        os.environ["DB_USER"] = database_connection_config["user"]
        os.environ["DB_PASSWORD"] = database_connection_config["password"]
        os.environ["DB_PORT"] = str(database_connection_config["port"])

    def unset_env():
        del os.environ["DB_HOST"]
        del os.environ["DB_USER"]
        del os.environ["DB_PASSWORD"]
        del os.environ["DB_PORT"]

    drop_table("guestlist")
    set_env()

    from app import application

    with application.test_client() as client:
        yield client

    unset_env()
    drop_table("guestlist")
